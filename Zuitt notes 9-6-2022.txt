git update-git-for-windows

git

local repositories - located in local computer

remote repositories - located in the internet

SSH Keys (Secure Shell) Key - tool that we can use to avoid entering our credentials everytime we need to log.

ssh-keygen



C:/Users/Admin/.ssh/id_rsa.pub
cat ~/.ssh/id_rsa.pub | clip

SSH Key

ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDn252CddP9RvYxTfMCJ2iq57Ev/aTKW3toN1hds1CFFL3sQoxKdBUbcIXiZVANURLxwORhIoTGDA/YmUDSHV9jeEiiaLTehJSzL0NSfwuAGzeGREKoRkCFheoekdLrlNLpEPgxCU+XH3/R13jqPYwIwU+2ZlxbBhm7X8sXQFbNEe9xuWvuiSqIQoqb92uPvBGA4uPFCgeE3Ccsw5yPnoqI4brjZTFUBMXsriza/aI3u9vKa633IPjjPBRRo1uoLZt4oZ/KxJKUpMVTUtyl4Vq7LtGXI5oooGzUAgA7IgHAXjEJ5lKH8ZmaWgpfpanpti3xBgh6DYXn56W4TjDv8+wlY9btglvdui6VEPRchGMO6yuICHkhh4LZAtBdO63BRTGUFO0vQOrpuKud5InLLd2jnZ0QkoQQWzz56RnSJXBorDdfeKBnV1LgAxbX/W151FlPRZCZM/g3Lf+XbwlQTaN9zekLHc0y13EJCe59lAMIjfHZqS5QQ2Jr+Fcvuv/sgPc= Admin@DESKTOP-J72PKVT

CLI (Command Line Interface) Commands

pwd = print working directory
ls = list of directories under the parent directory
mkdir "name" =  will create new folder
touch "name.txt" = will create a text file
cd batch-211 = change directory we are currently working on our CLI (terminal/gitbash)

Sublime Text 4
>>lightweight text/file editor
>>use less RAM/memory which is important because we also, as devs, have our Google Chrome open..

git config --global user.email "<emailFromGitlab/Github>"
--this allows us to identify the account from gitlab or github who will push/upload files into our online gitlab or github services

git config --global user.name "<usernameFromGitlab/Github>"